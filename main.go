package main

import (
	"bitbucket.org/felipsmartins/gomods/converters"
	"bitbucket.org/felipsmartins/gomods/converters/video"
	"fmt"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"net/http"
)

func main() {
	fmt.Println(converters.ReadBuffer())
	fmt.Println(video.Convert())

	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// Routes
	e.GET("/", hello)

	// Start server
	e.Logger.Fatal(e.Start(":1323"))
}

func hello(c echo.Context) error {
	return c.String(http.StatusOK, "Hello, World!")
}
